package calc

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import pl.kcz.calc.FileProcessor
import java.io.File

class FileProcessorTest {

    @ParameterizedTest(name = "Output of parsing {0}.in equals {0}.out")
    @ValueSource(strings = ["1-basic-arithmetic", "2-variables", "3-run"])
    fun processFilesTest(filename: String) {
        val output = mutableListOf<String>()

        val processor = FileProcessor { output.add(it) }
        processor.processFile("$filename.in")

        Assertions.assertEquals(
                File(ClassLoader.getSystemResource("$filename.out").toURI()).readLines(),
                output
        )
    }
}