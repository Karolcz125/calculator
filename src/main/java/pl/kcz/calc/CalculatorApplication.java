package pl.kcz.calc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class CalculatorApplication {
    private static String FILENAME = "1.big";

    public static void main(String... args) throws IOException {
        File outputFile = new File(FILENAME + ".out");

        FileOutputStream output = new FileOutputStream(outputFile);
        FileProcessor processor = new FileProcessor(s -> {
            try {
                output.write(s.getBytes());
                output.write('\n');
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        processor.processFile(FILENAME + ".in");
    }
}
