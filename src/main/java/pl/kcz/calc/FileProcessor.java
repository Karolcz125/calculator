package pl.kcz.calc;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FileProcessor {

    private Consumer<String> outputFunction;
    private int $ = 0;
    private Map<Character, Integer> variables = new LinkedHashMap<>();

    public FileProcessor(Consumer<String> outputFunction) {
        this.outputFunction = outputFunction;
        IntStream.range('a', 'z' + 1).forEach(it -> variables.put((char) it, 0));
    }

    public void processFile(String filename) throws FileNotFoundException {
        var resource = ClassLoader.getSystemResource(filename);

        if (resource == null) {
            throw new FileNotFoundException();
        }

        tryToProcessLines(resource);
    }

    private void tryToProcessLines(URL resource) {
        try (var reader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(resource.toURI()))))) {
            this.processLines(reader.lines());
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void processLines(Stream<String> lines) {
        lines
                .filter(it -> !it.isBlank())
                .map(line -> {
                    var res = processInstruction(line);
                    if (res != null) $ = res;
                    return res;
                })
                .map(it -> {
                    if (it == null) return "error";
                    else return "res " + it;
                })
                .forEach(outputFunction);
    }

    private Integer processInstruction(String line) {
        var args = line.split(" ");

        switch (args[0]) {
            case "add":
                return parseArgToInt(args[1]) + parseArgToInt(args[2]);
            case "sub":
                return parseArgToInt(args[1]) - parseArgToInt(args[2]);
            case "mul":
                return parseArgToInt(args[1]) * parseArgToInt(args[2]);
            case "div":
                if (parseArgToInt(args[2]) == 0) return null;
                return parseArgToInt(args[1]) / parseArgToInt(args[2]);
            case "set":
                variables.put(args[1].charAt(0), parseArgToInt(args[2]));
                return variables.get(args[1].charAt(0));
            case "run":
                try {
                    processFile(args[1]);
                    return $;
                } catch (FileNotFoundException e) {
                    return null;
                }
            default:
                return null;
        }
    }

    private int parseArgToInt(String arg) {
        if (arg.equals("$")) return $;
        else if (isNumber(arg)) return Integer.parseInt(arg);
        else return variables.get(arg.charAt(0));
    }

    private boolean isNumber(String str) {
        try {
            Integer.parseInt(str);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}